import React from "react";
import { Root } from "native-base";
import { createAppContainer, createDrawerNavigator, createStackNavigator } from "react-navigation";

import InitialScreen from "./screens/initial-screen";
import SideBar from "./screens/sidebar";
import AuthTabs from "./screens/tabs/"
import Main from "./screens/main/";
import Login from "./screens/login/";
import Signup from "./screens/signup/";
import SignupProfile from "./screens/signup-profile";
import ProductDetail from "./screens/product-detail";
import Chat from "./screens/chat";
import Search from "./screens/search"

const Stack = createStackNavigator(
  {
    InitialScreen: { screen: InitialScreen },
    Tabs: { screen: AuthTabs },
    Signup: { screen: Signup },
    SignupProfile: { screen: SignupProfile },
    ProductDetail: { screen: ProductDetail },
    Chat: { screen: Chat },
    Search: { screen: Search },
  },
  {
    initialRouteName: "InitialScreen",
    headerMode: "none",
  }
);

const AppNavigator = createAppContainer(
  createDrawerNavigator(
    {
      Stack: { screen: Stack },
      Main: { screen: Main },
      Login: { screen: Login },
    },
    {
      initialRouteName: "Stack",
      contentOptions: {
        activeTintColor: "#e91e63"
      },
      contentComponent: props => <SideBar {...props} />
    }
  )
);

export default () =>
  <Root>
    <AppNavigator />
  </Root>;
