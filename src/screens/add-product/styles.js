import { Dimensions } from 'react-native';

const imgWidth = Dimensions.get('screen').width / 2 - 14;

export default {
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  box: {
    width: imgWidth,
    margin: 2,
  },
  img: {
    height: imgWidth,
    width: imgWidth,
  },
  cam_img: {
    height: imgWidth - 40,
    width: imgWidth - 40,
    margin: 20,
  }
}