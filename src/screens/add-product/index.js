import React, { Component } from 'react';
import { View, Image, TouchableOpacity } from 'react-native';
import { Item, Input, Content, Button, Text, Label, Card, CardItem, Body, Picker, ListItem, CheckBox } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import { Field, reduxForm } from 'redux-form';
import { data } from '../../providers/api';
import styles from "./styles";

const validate = values => {

  const error = {};
  error.price = '';

  var price = values.price;

  if(values.email === undefined) {
    ema = '';
  }

  if(values.price === undefined) {
    price = '';
  }

  if(values.last_name === undefined) {
    ln = '';
  }

  if(ema.length < 4 && ema !== '') {
    error.email = 'too short';
  }

  if(!ema.includes('@') && ema !== '') {
    error.email = '@ not included';
  }

  if(price.length > 16) {
    error.price = 'max 16 characters';
  }

  if(ln.length > 16) {
    error.last_name = 'max 16 characters';
  }

  return error;
};

const options = {
  quality: 1.0,
  maxWidth: 500,
  maxHeight: 500,
  title: 'Select Photo',
  storageOptions: {
    skipBackup: true,
    // path: 'images',
  },
};

class AddProduct extends Component {

  constructor(props) {
    super(props);
    this.state = {
      status: 0,
      product_pic: [],
      product_data: [],
      pic_card: [],
      proof_img: null,
      proof_data: null,
      has_proof: 1,
      condition: 'Mint',
      price: 0,
      location_option: false,
      purchase_option: 0,
    }
  }

  renderInput = ({ input, label, type, meta: { touched, error, warning } }) => {
    var hasError= false;
    if(error !== undefined) {
      hasError= true;
    }
    return(
      <View>
        <Item floatingLabel error= {hasError}>
          <Label>{label}</Label>
          <Input {...input}/>
        </Item>
        {hasError ? <Text style={styles.error}>{error}</Text> : <Text />}
      </View>
    )
  }

  render() {
    const { handleSubmit, reset } = this.props;
    return (
      <Content padder>
        
        <View style={this.state.status != 0 ? {display: 'none'} : ''}>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text>Product Images</Text>
          </View>

          <View style={styles.container}>
            {this.state.pic_card}

            <TouchableOpacity onPress={this._getPhoto}>
              <Card style={styles.box}>
                <CardItem cardBody>
                  <Body>
                    <Image style={styles.cam_img} source = {require('../../../assets/camera.png')} />
                  </Body>
                </CardItem>
              </Card>
            </TouchableOpacity>
          </View>
          
          <Button style={{marginTop: 30}} block primary onPress={this._next}>
            <Text>Next</Text>
          </Button>
        </View>

        <View style={this.state.status != 1 ? {display: 'none'} : ''}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text>Proof of Authenticity or Receipt</Text>
            <TouchableOpacity onPress={this._getPhoto}>
              <Card style={styles.box}>
                <CardItem cardBody>
                  <Body>
                    <Image style={ this.state.proof_img != null ? styles.img : styles.cam_img} source = { this.state.proof_img != null ? this.state.proof_img : require('../../../assets/camera.png') } />
                  </Body>
                </CardItem>
              </Card>
            </TouchableOpacity>
          </View>
          
          <Button style={{marginTop: 30}} block primary onPress={this._next}>
            <Text>Next</Text>
          </Button>
        </View>

        <View padder style={this.state.status != 2 ? {display: 'none'} : ''}>

          <Field name="name" label="Product Name" component={this.renderInput} />

          <View style={{borderBottomWidth: 0.5, borderBottomColor: '#aaa', marginBottom: 10}}>
            <Text style={{color: '#666', marginLeft: 2, fontSize: 17, fontFamily: 'none'}}>Does the product comes with Proof of
Purchase?</Text>
            <Picker
              mode="dropdown"
              selectedValue={this.state.has_proof}
              onValueChange={this._profSelect}
            >
              <Item label="Yes" value="1" />
              <Item label="No" value="0" />
            </Picker>
          </View>

          <View style={{borderBottomWidth: 0.5, borderBottomColor: '#aaa', marginBottom: 20}}>
            <Text style={{color: '#666', marginLeft: 2, fontSize: 17, fontFamily: 'none'}}>Condition of Item?</Text>
            <Picker
              mode="dropdown"
              selectedValue={this.state.condition}
              onValueChange={this._conditionSelect}
            >
              <Item label="Mint" value="Mint" />
              <Item label="Good" value="Good" />
              <Item label="Used" value="Used" />
              <Item label="Damaged" value="Damaged" />
            </Picker>
          </View>

          <Field name="price" label="Price" component={this.renderInput} />

          <View style={{borderBottomWidth: 0.5, borderBottomColor: '#aaa', marginBottom: 20}}>
            <Text style={{color: '#666', marginLeft: 2, fontSize: 17, fontFamily: 'none'}}>Location Options</Text>
            <ListItem style={{borderBottomWidth: 0}} button onPress={() => this.toggleSwitch(false)}>
              <CheckBox
                color="red"
                checked={!this.state.location_option}
                onPress={() => this.toggleSwitch(false)}
              />
              <Body>
                <Text>Within the country</Text>
              </Body>
            </ListItem>

            <ListItem style={{borderBottomWidth: 0}} button onPress={() => this.toggleSwitch(true)}>
              <CheckBox
                color="red"
                checked={this.state.location_option}
                onPress={() => this.toggleSwitch(true)}
              />
              <Body>
                <Text>Ship to location_option</Text>
              </Body>
            </ListItem>
          </View>

          <View style={{borderBottomWidth: 0.5, borderBottomColor: '#aaa', marginBottom: 20}}>
            <Text style={{color: '#666', marginLeft: 2, fontSize: 17, fontFamily: 'none'}}>Purchase Options</Text>
            <Picker
              mode="dropdown"
              selectedValue={this.state.purchase_option}
              onValueChange={this._purchaseOptionSelect}
            >
              <Item label="Pickup (Cash on pickup)" value="0" />
              <Item label="Paypal or Proof of Bank Transfer for Delivery/Mail" value="1" />
            </Picker>
          </View>

          <Button style={{marginTop: 30}} block primary onPress={handleSubmit(this._handleSubmit)}>
            <Text>Submit</Text>
          </Button>
        </View>
      </Content>
    );
  }

  _getPhoto = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
        if(this.state.status == 0) {
          let temp = this.state.product_pic;
          let data_temp = this.state.product_data;
          temp.push(source);
          data_temp.push(response.data);
          this.setState({
            product_pic: temp,
            product_data: data_temp
          });
  
          let temp2 = this.state.pic_card;
          temp2.push(
            <Card style={styles.box}>
              <CardItem cardBody>
                <Body>
                  <Image style={styles.img} source = {source} />
                </Body>
              </CardItem>
            </Card>
          );
          this.setState({ pic_card: temp2 })
        } else {
          this.setState({
            proof_img: source,
            proof_data: response.data
          });
        }
      }
    })
  }

  _profSelect = value => {
    this.setState({
      has_proof: value
    });
  }

  _conditionSelect = value => {
    this.setState({
      condition: value
    });
  }

  _purchaseOptionSelect = value => {
    this.setState({
      purchase_option: value
    });
  }

  toggleSwitch(value) {
    this.setState({
      location_option: value
    });
  }
  
  _next = () => {
    this.setState({status: this.state.status + 1});
  }

  _handleSubmit = values => {
    data.post('create-new-product', {
      name: values.name,
      condition: this.state.condition,
      has_proof: this.state.has_proof,
      location_option: this.state.location_option,
      price: values.price,
      purchase_option: this.state.purchase_option
    }).then(res =>{
      console.log('testing result', res);
      let prod_id = res.data.productInfo.id;

      this.state.product_data.forEach(img => {
        this._uploadImg(img, {productId: prod_id});
      });
      
      this._uploadProof(this.state.proof_data, {productId: prod_id});

      this.props.initialValues.tab.setModalVisible(false);
      console.log('foooo!');
    }).catch((err)=>{
      //
    })
    
  }

  _uploadImg = (img, info) => {
    data.upload('upload-image', img, info).then((res)=>{
      console.log('_uploadImg result', res)
    }).catch((err)=>{
      //
    })
  }

  _uploadProof = (img, info) => {
    data.upload('upload-proof', img, info).then((res)=>{
      console.log('_uploadProof result', res)
    }).catch((err)=>{
      //
    })
  }

}

export default reduxForm({
  form: 'addProduct',
  validate
})(AddProduct)