import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import {
  Content,
  List,
  ListItem,
  Text,
  Left,
  Right,
  Badge,
} from "native-base";
import styles from "./styles";

const datas = [
  "Simon Mignolet",
  "Nathaniel Clyne",
  "Dejan Lovren",
  "Mama Sakho",
  "Alberto Moreno",
  "Emre Can",
  "Joe Allen",
  "Phil Coutinho"
];

export default class Inbox extends Component {
  render() {
    return (
      <ScrollView>
        <Content>
          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem>
                <Left>
                  <Text>
                    {data}
                  </Text>
                </Left>
                <Right>
                <Badge>
                  <Text>2</Text>
                </Badge>
                </Right>
              </ListItem>}
          />
        </Content>
      </ScrollView>
    )
  }
};