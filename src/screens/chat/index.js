import React, { Component } from 'react';
import { View, ScrollView, RefreshControl } from 'react-native';
import { Container, Text, Header, Body, Left, Right, Title, Button, Icon, ListItem, Thumbnail, Input, Footer } from "native-base";
import styles from './styles';

export default class Chat extends Component {

  constructor(props) {
    super(props)
    this.state = {
      refreshing: false,
    }
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    // this._getProdImages();
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.pop()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Chat</Title>
          </Body>
          <Right/>
        </Header>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>

          <View style={styles.chatbox}>
            <Thumbnail style={styles.img} source={require('../../../assets/skeleton.jpg')}/>
            <Text style={styles.msg}>Testing</Text>
          </View>
          <View style={styles.chatbox}>
            <Thumbnail style={styles.img} source={require('../../../assets/skeleton.jpg')}/>
            <Text style={styles.msg}>Testing2</Text>
          </View>
          <View style={styles.chatbox}>
            <Text style={styles.msg}>Testing3</Text>
            <Thumbnail style={styles.img} source={require('../../../assets/skeleton.jpg')}/>
          </View>
        </ScrollView>

        <Footer style={{backgroundColor: '#fff'}}>
          <View style={{flex:1, flexDirection: 'row', marginTop: 5}}>
            <Input style={{backgroundColor: '#eee', marginHorizontal: 10, borderRadius: 20, height: 45}} placeholder='message' />
            <Button style={{borderRadius: 20, marginRight: 10}}>
              <Text>Send</Text>
            </Button>
          </View>
        </Footer>
      </Container>
    );
  }
}
