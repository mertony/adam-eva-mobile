export default {
  chatbox: {
    flex:1,
    padding: 5,
    flexDirection: 'row',
  },
  img: {
    width: 46,
    height: 46,
  },
  msg: {
    flex: 1,
    marginHorizontal: 5,
    padding: 10,
    borderRadius: 20,
    backgroundColor: '#777',
  }
}