import React, { Component } from "react";
import { Alert, AsyncStorage } from 'react-native';
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge
} from "native-base";
import styles from "./style";

const datas = [
  {
    name: "Main",
    route: "Main",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "Login",
    route: "Login",
    icon: "arrow-up",
    bg: "#477EEA",
    types: "11"
  }
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
  }

  render() {
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
        >
          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}
              >
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: "#777", fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
                </Left>
                {data.types &&
                  <Right style={{ flex: 1 }}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg
                      }}
                    >
                      <Text
                        style={styles.badgeText}
                      >{`${data.types} Types`}</Text>
                    </Badge>
                  </Right>}
              </ListItem>}
          />
          <List>
            <ListItem
              button
              noBorder
              onPress={this._logout}
            >
              <Left>
                <Icon
                  active
                  type="FontAwesome"
                  name="sign-out"
                  style={{ color: "#777", fontSize: 26, width: 30 }}
                />
                <Text style={styles.text}>
                  Logout
                </Text>
              </Left>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }

  _logout = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Yes', onPress: () => {
          AsyncStorage.removeItem('socialtoken');
          this.props.navigation.navigate('Main');
        }},
      ],
      { cancelable: false }
    )
    this.props.navigation.closeDrawer();
  }
}

export default SideBar;
