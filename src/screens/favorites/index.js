import React, { Component } from 'react';
import { Card, CardItem, Body, Text, Left, Thumbnail } from 'native-base';
import { Image, View, ScrollView, RefreshControl, TouchableOpacity } from 'react-native';
import { data } from '../../providers/api';
import { APP_CONFIG } from '../../config/global';
import moment from 'moment';
import styles from "./styles";

export default class Favorites extends Component
{
  state = {
    prod: [],
    tempProd: [],
    refreshing: false,
  }

  constructor(props)
  {
    super(props);
  }

  componentDidMount()
  {
    this._makeTempProd();
    this._getFavorites();
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this._getFavorites();
  }

  render()
  {
    return (
      <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
        />
      }>
        <View style={styles.container}>
          {this.state.prod == '' ? this.state.tempProd : this.state.prod}
        </View>
      </ScrollView>
    )
  }

  _makeTempProd()
  {
    let temp = [];
    for (let index = 0; index < 10; index++) {
      temp.push(
        <Card key={index} style={styles.box}>
          <CardItem>
            <Left>
              <Thumbnail style={styles.thumbnail} source={require('../../../assets/skeleton.jpg')} />
              <Body>
                <Text style={styles.text}></Text>
                <Text style={styles.subtext}></Text>
              </Body>
            </Left>
          </CardItem>

          <CardItem cardBody>
            <Body>
              <Image style={styles.img} source = {require('../../../assets/skeleton.jpg')} />
            </Body>
          </CardItem>
        </Card>
      )
    }
    this.setState({tempProd: temp});
  }

  _getFavorites()
  {
    console.log('Calling this. reacting')
    data.get('favorites', {offset: 0, search: ''}).then((res)=>{
      var prod = res.map((prop, key) => {
        return (
          <Card key={key} style={styles.box}>
            <CardItem style={{height: 50}}>
              <Left>
                <Thumbnail style={styles.thumbnail} source={{uri:prop.picture}} />
                <Body>
                  <Text style={styles.text}>{prop.first_name}</Text>
                  <Text style={styles.subtext}>{moment(prop.created_at).fromNow()}</Text>
                </Body>
              </Left>
            </CardItem>

            <TouchableOpacity onPress={() => this.props.tabnav.push('ProductDetail', prop)}>
              <CardItem cardBody>
                <Body>
                  <Image style={styles.img} source = {{uri:APP_CONFIG.PROD_IMG_URL+prop.value}} />
                </Body>
              </CardItem>
            </TouchableOpacity>
          </Card>
        );
      });
      this.setState({prod: prod, refreshing: false});
      console.log('Favorites result', res)
    }).catch((err)=>{
      console.log(err);
      this.setState({refreshing: false});
    })
  }
}