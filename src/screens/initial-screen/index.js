import React, { Component } from 'react';
import { View, AsyncStorage } from 'react-native';

export default class InitialScreen extends Component {

  constructor(props) {
    super(props);
  }
  
  async componentWillMount() {
    const token = await AsyncStorage.getItem('socialtoken');
    if(token !== null) {
      this.props.navigation.navigate('Tabs');
    } else {
      this.props.navigation.navigate('Main');
    }
  }

  render() {
    return (
      <View>
        
      </View>
    );
  }
}
