export default {
  thumbnail: {
    height: 100,
    width: 100,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 50,
  },
  image: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  error: {
    color: '#d43d20',
    marginBottom: 10,
  }
}