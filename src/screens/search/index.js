import React, { Component } from 'react';
import { View, StatusBar, ScrollView, RefreshControl, TouchableOpacity, Image } from 'react-native';
import { Container, Header, Icon, Body, Left, Item, Input, Text, Button, Card, CardItem, Thumbnail } from "native-base";
import { data } from '../../providers/api';
import { APP_CONFIG } from '../../config/global';
import moment from 'moment';
import styles from "./styles";

export default class Search extends Component {

  state = {
    prod: [],
    tempProd: [],
    search: this.props.navigation.state.params.search,
    refreshing: false,
  }

  constructor(props)
  {
    super(props);
  }

  componentDidMount()
  {
    this._makeTempResult();
    this._getResult();
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this._getResult();
  }

  render() {
    return (
      <Container>
        <Header rounded>
          <Button transparent onPress={() => this.props.navigation.pop()}>
            <Icon name="arrow-back" />
          </Button>
          <Body>
            <Item style={{ backgroundColor: "#fff", borderRadius: 5, }}>
              <Icon style={{ marginLeft: 5 }} active name="search" />
              <Input placeholder="Search" returnKeyType="search" value={this.state.search} onSubmitEditing={ event => this._getResult(event.nativeEvent.text)}/>
            </Item>
          </Body>
        </Header>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
          <View style={styles.container}>
            <StatusBar
              barStyle="light-content"
            />
            { this.state.prod == '' ? this.state.tempProd : this.state.prod }
          </View>
        </ScrollView>
      </Container>
    )
  }

  _makeTempResult()
  {
    let temp = [];
    for (let index = 0; index < 10; index++) {
      temp.push(
        <Card key={index} style={styles.box}>
          <CardItem style={{height: 50}}>
            <Left>
              <Thumbnail style={styles.thumbnail} source={require('../../../assets/skeleton.jpg')} />
              <Body>
                <Text style={styles.text}></Text>
                <Text style={styles.subtext}></Text>
              </Body>
            </Left>
          </CardItem>

          <CardItem cardBody>
            <Body>
              <Image style={styles.img} source = {require('../../../assets/skeleton.jpg')} />
            </Body>
          </CardItem>
        </Card>
      )
    }
    this.setState({tempProd: temp});
  }

  _getResult = () => {
    console.log('Calling this. reacting')
    data.post('product-search', {search: this.state.search}).then((res)=>{
      var prod = res.data.map((prop, key) => {
        return (
          <Card key={key} style={styles.box}>
            <CardItem style={{height: 50}}>
              <Left>
                <Thumbnail style={styles.thumbnail} source={{uri:prop.picture}} />
                <Body>
                  <Text style={styles.text}>{prop.first_name}</Text>
                  <Text style={styles.subtext}>{moment(prop.created_at).fromNow()}</Text>
                </Body>
              </Left>
            </CardItem>

            <TouchableOpacity onPress={() => this.props.navigation.push('ProductDetail', prop)}>
              <CardItem cardBody>
                <Body>
                  <Image style={styles.img} source = {{uri:APP_CONFIG.PROD_IMG_URL+prop.value}} />
                </Body>
              </CardItem>
            </TouchableOpacity>
          </Card>
        );
      });
      this.setState({prod: prod, refreshing: false});
      console.log('Products result', res.data)
    }).catch((err)=>{
      console.log(err);
      this.setState({refreshing: false});
    })
  }
};