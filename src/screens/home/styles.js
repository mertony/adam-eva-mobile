import { Dimensions } from 'react-native';

const imgWidth = Dimensions.get('screen').width / 2 - 4;

export default {
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  box: {
    width: imgWidth,
    margin: 2,
  },
  img: {
    height: imgWidth,
    width: imgWidth,
  },
  text: {
    fontSize: 10,
  },
  subtext: {
    fontSize: 9,
    color: '#666',
  },
  thumbnail: {
    height: 40,
    width: 40,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 50,
  }
}