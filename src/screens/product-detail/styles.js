import { Dimensions } from 'react-native';

const scrnWidth = Dimensions.get('screen').width;

export default {
  box: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#aaa',
  },
  img: {
    height: scrnWidth > 720 ? 720 : scrnWidth,
    width: scrnWidth > 720 ? 720 : scrnWidth,
  },
  name: {
    fontSize: 16,
    marginVertical: 10,
    marginHorizontal: 20,
    fontFamily: 'none',
  },
  detail: {
    fontSize: 14,
    color: '#666',
    fontFamily: 'none',
  },
  subdetail: {
    fontSize: 14,
    color: '#666',
    marginBottom: 10,
    fontFamily: 'none',
    marginLeft: 20,
  },
  floatbtn: {
    flex: 1,
    bottom: 10,
    borderRadius: 5,
    position: 'absolute',
    flexDirection: 'row',
    left: scrnWidth /2 -125,
    backgroundColor: '#d43d20',
  }
}