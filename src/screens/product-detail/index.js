import React, { Component } from 'react';
import { Image, ScrollView, RefreshControl, View } from 'react-native';
import { Container, Text, Header, Body, Left, Right, Title, Button, Icon, ListItem } from "native-base";
import ImageSlider from 'react-native-image-slider';
import { APP_CONFIG } from '../../config/global';
import { data } from '../../providers/api';
import moment from 'moment';
import styles from "./styles";

export default class ProductDetail extends Component {

  constructor(props)
  {
    super(props);
    this.state = {
      imgs: [],
      refreshing: false,
      active: false,
      liked: false,
      likes: 0,
    }
  }

  componentDidMount()
  {
    this._makeTempImages();
    this._getProdImages();
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this._getProdImages();
  }

  render() {
    let detail = this.props.navigation.state.params;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.pop()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Product Detail</Title>
          </Body>
          <Right/>
        </Header>

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>

          <ImageSlider images={this.state.imgs}
            customSlide={({ index, item, style, width }) => (
              <View key={index} style={[style, styles.box]}>
                <Image style={[style, styles.img]} source = {item} />
              </View>
            )}
          />

          <View>
            <Text style={styles.name}>{detail.name}</Text>

            <ListItem icon noBorder noIndent>
              <Left>
                <Icon type="FontAwesome" name="clock-o" />
              </Left>
              <Body>
                <Text style={styles.detail}>{moment(detail.created_at).fromNow()}</Text>
              </Body>
            </ListItem>
            
            <ListItem icon noBorder>
              <Left>
                <Icon type="FontAwesome" name="money" />
              </Left>
              <Body>
                <Text style={styles.detail}>{detail.price}</Text>
              </Body>
            </ListItem>

            <ListItem icon noBorder>
              <Left>
                <Icon type="FontAwesome" name="dropbox" />
              </Left>
              <Body>
                <Text style={styles.detail}>{detail.condition}</Text>
              </Body>
            </ListItem>

            <ListItem icon noBorder>
              <Left>
                <Icon type="FontAwesome" name="list" />
              </Left>
              <Body>
                <Text style={styles.detail}>Category</Text>
              </Body>
            </ListItem>

            <ListItem icon noBorder>
              <Left>
                <Icon type="FontAwesome" name="info-circle" />
              </Left>
              <Body>
                <Text style={styles.detail}>Info</Text>
              </Body>
            </ListItem>

            <ListItem icon noBorder>
              <Left/>
              <Body>
                <Text style={styles.subdetail}>Brand</Text>
              </Body>
            </ListItem>

            <ListItem icon noBorder>
              <Left/>
              <Body>
                <Text style={styles.subdetail}>{detail.first_name}</Text>
              </Body>
            </ListItem>
          </View>
        </ScrollView>

        <View style={styles.floatbtn}>
          <Button style={{borderBottomLeftRadius: 5, borderTopLeftRadius: 5, borderBottomRightRadius: 0, borderTopRightRadius: 0, backgroundColor: '#fff'}} onPress={this.likeToggle}>
            <Icon style={{color: this.state.liked ? '#d43d20' : '#777'}} type="FontAwesome" name="heart">
              <Text style={{marginLeft:2}}>{this.state.likes}</Text>
            </Icon>
          </Button>

          <Button onPress={() => this.props.navigation.push('Chat')}>
            <Text>Chat</Text>
          </Button>

          <Button style={{borderBottomRightRadius: 5, borderTopRightRadius: 5}}>
            <Text>Make Offer</Text>
          </Button>
        </View>

      </Container>
    )
  }

  _makeTempImages = () => {
    let temp = [];
    for (let index = 0; index < 3; index++) {
      temp.push(require('../../../assets/skeleton.jpg'));
    }
    this.setState({imgs: temp});
  }

  _getProdImages = () => {
    data.post('product-imgs', {prod_id: this.props.navigation.state.params.prod_id}).then((res)=>{
      var images = res.data.imgs.map((prop, key) => { return {uri: APP_CONFIG.PROD_IMG_URL+prop.value} });
      this.setState({imgs: images, likes: res.data.count, liked: res.data.liked, refreshing: false});
      console.log('_getProdImages result', res);
    }).catch((err)=>{
      this.setState({refreshing: false});
    })
  }

  likeToggle = () => {
    data.post('like-product', {prod_id: this.props.navigation.state.params.prod_id}).then((res)=>{
      console.log('likeToggle result', res);
      this.setState({likes: res.data.count, liked: res.data.liked});
    }).catch((err)=>{
      console.log(err);
    })
  }
};