import React, { Component } from 'react';
import { Alert, Modal } from 'react-native';
import { Container, Header, Title, Button, Icon, Tabs, Tab, Body, Left, Right, Content, Item, Input, Fab } from "native-base";
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import allReducers from '../../reducers';
import Home from '../home';
import Inbox from '../inbox';
import Profile from '../profile';
import Favorites from '../favorites';
import AddProduct from '../add-product';
const store = createStore(allReducers);

export default class AuthTabs extends Component {
  state = {
    modalVisible: false,
  };

  render() {
    return (
      <Container>
        <Header rounded>
          <Body>
            <Item style={{ backgroundColor: "#fff", borderRadius: 5, }}>
              <Icon style={{ marginLeft: 5 }} active name="search" />
              <Input placeholder="Search" returnKeyType="search" onSubmitEditing={ event => this.doSearch(event.nativeEvent.text)}/>
            </Item>
          </Body>
          <Button transparent>
            <Icon type="FontAwesome" active name="bell" />
          </Button>
          <Button transparent>
            <Icon type="FontAwesome" active name="bars" onPress={ () => this.props.navigation.toggleDrawer() }/>
          </Button>
        </Header>

        <Tabs>
          <Tab heading="Home">
            <Home tabnav={this.props.navigation}/>
          </Tab>
          <Tab heading="Favorites">
            <Favorites tabnav={this.props.navigation}/>
          </Tab>
          <Tab heading="Inbox">
            <Inbox />
          </Tab>
          <Tab heading="Profile">
            <Profile />
          </Tab>
        </Tabs>

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => this.setModalVisible(!this.state.modalVisible) }>
          <Header rounded>
            <Left />
            <Body>
              <Title>Add Product</Title>
            </Body>
            <Right>
              <Button transparent onPress={ () => this.setModalVisible(!this.state.modalVisible) }>
                <Icon type="FontAwesome" active name="close" />
              </Button>
            </Right>
          </Header>

          <Provider store={store}>
            <AddProduct initialValues={{tab: this}}/>
          </Provider>
        </Modal>

        <Fab direction="up" position="bottomRight" style={{ backgroundColor: '#d43d20' }} onPress={() => this.setModalVisible(true)}>
          <Icon type="FontAwesome" active name="camera"/>
        </Fab>
      </Container>
    );
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  doSearch = value => {
    this.props.navigation.push('Search', {search: value});
  }

}