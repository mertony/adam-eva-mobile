import React, { Component } from 'react';
import { Card, CardItem, Body, Text, Left, Thumbnail } from 'native-base';
import { Image, View, ScrollView, TouchableOpacity, RefreshControl } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { APP_CONFIG } from '../../config/global';
import { data } from '../../providers/api';
import styles from "./styles";

const options = {
  quality: 1.0,
  maxWidth: 500,
  maxHeight: 500,
  title: 'Select Photo',
  storageOptions: {
    skipBackup: true,
    // path: 'images',
  },
};


export default class Profile extends Component
{
  constructor(props) {
    super(props);

    this.state = {
      prod: [],
      tempProd: [],
      num: 0,
      selected: [],
      profile_photo: null,
      name: '',
      modalVisible: false,
      refreshing: false,
    }

  }

  componentDidMount()
  {
    this._makeTempProd();
    this.getProfile();
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.getProfile();
  }

  render() {
    return (
      <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
        />
      }>
        <View>
          <Card>
            <CardItem>
              <Left>
                <Body>
                  <Text style={styles.text}>{this.state.name}</Text>
                  <Text style={styles.subtext}>3/F, Stary Building, 9000, Max Y. Suniel St, Cagayan de Oro, Misamis Oriental</Text>
                </Body>
                <TouchableOpacity>
                  <Thumbnail style={styles.thumbnail} source={this.state.profile_photo === null ? require('../../../assets/skeleton.jpg') : this.state.profile_photo} />
                </TouchableOpacity>
              </Left>
            </CardItem>

            <CardItem>
              <Body>
                <Text style={styles.description}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae esse assumenda itaque odit minima architecto perferendis placeat natus? Ipsa maiores quia, placeat esse cum magnam facere culpa eius sapiente fuga?</Text>
              </Body>
            </CardItem>
          </Card>

          <View style={styles.container}>
            { this.state.prod == '' ? this.state.tempProd : this.state.prod }
          </View>
        </View>
      </ScrollView>
    )
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  getPhoto = () => {
      ImagePicker.showImagePicker(options, (response) => {
        console.log('Response = ', response);
      
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          const source = { uri: response.uri };
      
          // You can also display the image using data:
          // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      
          this.setState({
            profile_photo: source,
          });
        }
    })
  }

  _makeTempProd()
  {
    let temp = [];
    for (let index = 0; index < 10; index++) {
      temp.push(
        <Card key={index} style={styles.box}>
          <CardItem>
            <Body>
              <Text style={styles.prodtext}>Product</Text>
            </Body>
          </CardItem>

          <CardItem cardBody>
            <Body>
              <Image style={styles.img} source = {require('../../../assets/skeleton.jpg')} />
            </Body>
          </CardItem>
        </Card>
      )
    }
    this.setState({tempProd: temp});
  }

  getProfile = () => {
    console.log('Getting profile');
    data.get('profile-data').then((res)=>{
      console.log('getProfile result', res);
      this.setState({
        name: res.profile.first_name + ' ' + res.profile.last_name,
        profile_photo: { uri: res.profile.picture },
      });
      let temp = res.products.map((prop, key) => {
        return (
          <Card key={key} style={styles.box}>
            <CardItem>
              <Body>
                <Text style={styles.subtext}>{prop.name}</Text>
              </Body>
            </CardItem>

            <CardItem cardBody>
              <Body>
                <Image style={styles.img} source = {{uri:APP_CONFIG.PROD_IMG_URL+prop.value}} />
              </Body>
            </CardItem>
          </Card>
        );
      });
      this.setState({prod: temp});
      this.setState({refreshing: false});
    }).catch((err)=>{
      this.setState({refreshing: false});
    })
  }

};