import { Dimensions } from 'react-native';

const imgWidth = Dimensions.get('screen').width / 2 - 4;

export default {
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  box: {
    width: imgWidth,
    margin: 2,
  },
  img: {
    height: imgWidth,
    width: imgWidth,
  },
  text: {
    fontSize: 14,
  },
  subtext: {
    fontSize: 11,
    color: '#666',
  },
  prodtext: {
    fontSize: 11,
  },
  description: {
    color: '#444',
  },
  thumbnail: {
    height: 100,
    width: 100,
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 50,
  },

  container2: {
    flex: 1,
    backgroundColor: '#F6AE2D',
  },
  content: {
    marginTop: 15,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  text2: {
    fontSize: 16,
    alignItems: 'center',
    color: '#fff',
  },
  bold: {
    fontWeight: 'bold',
  },
  info: {
    fontSize: 12,
  },
}